interface Usuario {
    val nombre: String
    val apellido: String
    fun getFullName() = "$nombre $apellido"
    fun estado(): String
}

enum class TipoUsuario {
    Normal,
    Premium
}


object UsuarioFactory {
    fun getUsuario(userType: TipoUsuario, nombre: String, apellido: String): Usuario {
        return when (userType) {
            TipoUsuario.Normal -> Normal(nombre = nombre, apellido = apellido)
            TipoUsuario.Premium -> Premium(nombre = nombre, apellido = apellido)
        }
    }
}


class Normal(override val nombre: String, override val apellido: String) : Usuario {
    override fun estado() = "Normal"

}

class Premium(override val nombre: String, override val apellido: String) : Usuario {
    override fun estado() = "Premium"

}

fun main() {
    val normal = UsuarioFactory.getUsuario(TipoUsuario.Normal, "Anderson", "Revelo")
    with(normal) {
        println(getFullName())
        println(estado())

    }


    val premium = UsuarioFactory.getUsuario(TipoUsuario.Premium, "Fabian", "Morillo")
    with(premium) {
        println(getFullName())
        println(estado())

    }
}